---

title: "2020-10 Release Announcement"
date: 2020-10-12
author: vmbrasseur
excerpt: "Announcing the 2020-10 release of the FOSS Governance Collection"
layout: single
permalink: /2020/10/12/2020-10-release-announcement

---

Please welcome the first FOSS Governance Collection release to the world!

The current plan is to do a new release every month. Releases are named for the year-month they're launched, so this one is 2020-10.

What will you find in 2020-10? A heckuva lot more than we were expecting! 

You folks have suggested a lot of great documents to add. When adding those, we often have a quick look around the project to see what other governance-related documents they have lying about. Thanks to your suggestions, 2020-10 features **57 new governance documents** in the collection.

There are also several site-related changes. We've added a [contributors](https://fossgovernance.org/contributors/) page to thank the many people who are helping improve the project and a [release notes](https://fossgovernance.org/release-notes/) page to keep track of all those improvements. We've converted the [launch blog post](https://fossgovernance.org/2020/09/21/getting-started-with-the-foss-governance-collection) into a new [Getting Started guide](https://fossgovernance.org/getting-started/). We've also updated the [Contributing guide](https://fossgovernance.org/contribute/) with more information. Hopefully this will make it easier for people to lend a hand.

Please have a look at [the release notes](https://fossgovernance.org/release-notes/#2020-10) for complete details and as always, [keep those suggestions coming](https://fossgovernance.org/contribute/#ways-to-contribute)! The more documents we add to the collection, the more useful it is for all of us.
